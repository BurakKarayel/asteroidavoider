using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{

    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private float scoreMultiplier;


    private float score;

    private bool shouldCount = true;

        public const string HighScoreKey = "HighScore";
        
    

    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        if(!shouldCount){return;}
        score += scoreMultiplier * Time.deltaTime;

        scoreText.text = Mathf.FloorToInt(score).ToString();
        

    }

    public void StartTimer()
    {
        shouldCount = true;
    }

    public int EndTimer()
    {
        shouldCount = false;
        scoreText.text = string.Empty; 

        int currentHighScore = PlayerPrefs.GetInt(HighScoreKey, 0);

    if(score > currentHighScore)
    {
        PlayerPrefs.SetInt("HighScore", Mathf.FloorToInt(score));
    

    }

        return Mathf.FloorToInt(score);



        
    }




}
