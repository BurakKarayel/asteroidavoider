using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Firebase.Analytics;
using UnityEngine.Events;

public class GameOverHandler : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Button continueButton; 
    [SerializeField] private TMP_Text gameOverText;
    [SerializeField] private ScoreSystem scoreSystem;
    [SerializeField] private GameObject gameOverDisplay;
    [SerializeField] private AstroidSpawner astroidSpawner;

    [SerializeField] private AdManager_Admob1 interstitialAd;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioSource audioSourceFX;

    public UnityEvent showInterstitial;

    
    public void EndGame()
    {
        astroidSpawner.enabled = false;

        int finalScore = scoreSystem.EndTimer();

        gameOverText.text = "Your Score: " + finalScore;
        //FirebaseAnalytics.LogEvent("level_score","points",finalScore);

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPostScore,FirebaseAnalytics.ParameterScore,finalScore);

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd);

        gameOverDisplay.gameObject.SetActive(true);

                audioSource.GetComponent<AudioSource>().enabled = false;
                audioSourceFX.GetComponent<AudioSource>().enabled = true;

        


    


    }
    public void PlayAgain()
    
    {
        showInterstitial.Invoke();
        gameOverDisplay.gameObject.SetActive(false);
        
        
    
        Invoke("SceneChange",1);
    }

    public void SceneChange()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowAd()
    {
        interstitialAd.OnClickShowGameSceneButton();
    }

    public void ContinueButton()
    {
        //AdManager.Instance.ShowAd(this);
        continueButton.interactable = false;
        
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ContinueGame()
    {
        scoreSystem.StartTimer();
        continueButton.interactable = false;



        player.transform.position = Vector3.zero;
        player.SetActive(true);
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;

        astroidSpawner.enabled = true;

        gameOverDisplay.gameObject.SetActive(false);

        audioSource.GetComponent<AudioSource>().enabled = true;
        audioSourceFX.GetComponent<AudioSource>().enabled = false;


    }
}
