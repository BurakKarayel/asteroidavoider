using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.Events;
using GoogleMobileAds.Common;
using GoogleMobileAds.Placement;



public class AdManager_Admob1 : MonoBehaviour
{
    // Start is called before the first frame update

[SerializeField] public GameObject button;

RewardedAdGameObject rewardedAd;
BannerAdGameObject bannerAd;

InterstitialAdGameObject interstitialAd;



    
    void Start()
    {
        //button.SetActive(false);

        //rewardedAd = MobileAds.Instance.GetAd<RewardedAdGameObject>("rewarded");

        MobileAds.Instance.GetAd<BannerAdGameObject>("BannerAd");
        interstitialAd = MobileAds.Instance
            .GetAd<InterstitialAdGameObject>("InterstitialAd");
        
        //bannerAd.LoadAd();
        //bannerAd.Show();
        


        // Initialize the Mobile Ads SDK.
        MobileAds.Initialize((initStatus) =>
        {
            Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
            foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
            {
                string className = keyValuePair.Key;
                AdapterStatus status = keyValuePair.Value;
                switch (status.InitializationState)
                {
                case AdapterState.NotReady:
                    // The adapter initialization did not complete.
                    MonoBehaviour.print("Adapter: " + className + " not ready.");
                    break;
                case AdapterState.Ready:
                    // The adapter was successfully initialized.
                    MonoBehaviour.print("Adapter: " + className + " is initialized.");
                    break;
                }
            }
        });

            

        
    }


    

    public void OnUserEarnedReward(Reward reward) {
        Debug.Log("OnUserEarnedReward: reward=" +
            reward.Type + ", amount=" + reward.Amount);
            rewardedAd.LoadAd();


            

    }

    public void OnBannerAdFailedToLoad(string reason) {
    Debug.Log("Banner ad failed to load: " + reason);
}


public void OnClickShowGameSceneButton()
    {
        // Display an interstitial ad
        interstitialAd.ShowIfLoaded();


    }

public void AdLoaded(){

    
    Debug.Log("Ad Loaded");
    }
    }
